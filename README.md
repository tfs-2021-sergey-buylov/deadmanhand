# Deadmanhand
Deadmanhand is kind of heartbeat service. It receives simple http request, and if they missed longer then timeout - send message to given telegram channel. Also it has api for list,create and delete checks. Encouraged by https://deadmansnitch.com and some other monitoring tools.

##API examples:

curl localhost:8080/api/list - List all from DB

curl localhost:8080/api/update/73198711-5c6f-4b81-b9ec-b24f4417680e -f (-f stands for non-zero exit code if http response code bigger than 400)
Update lastcheckin field to current timestamp

curl -X POST -H 'Content-Type: application/json' localhost:8080/api/add -d '{"name":"check2","timeout":6000,"description":"ALARM!ASAP!","notification":"-1001293046572"}'
Generate uuid for the check and add row to database

curl -X DELETE localhost:8080/api/del/424f46f9-f6e0-450c-bd03-8221533c63ef

Delete check from db

## Technologies:
Slick , Akka-http, sqlite

## Problems:
- poor naming
- blocking code for the Check (Thread.sleep instead of some kind of Scheduller)
- non-informative responses for http calls
