package sb

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import com.typesafe.scalalogging.LazyLogging
import sb.api.Api
import sb.logic.Checker
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
object DeadManHttpApp {
  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = ac.dispatcher
  def main(args: Array[String]): Unit = {
    Await.result(DeadManHand().start(), Duration.Inf)
    ()
  }
}

case class DeadManHand()(implicit ac: ActorSystem, ec: ExecutionContext) extends LazyLogging {

  private val routes = Route.seal(
    RouteConcatenation.concat(
      Api.route
    )
  )

  def start(): Future[Http.ServerBinding] = {
    Checker.badMainLoop
    Http()
      .newServerAt("localhost", 8080)
      .bind(routes)
      .andThen { case b => logger.info(s"server started at: $b") }
  }
}