package sb
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

import java.util.UUID

case class MyCheck(uuid: UUID,
                   name:String,
                   timeout: Int,
                   description: String,
                   notification: String,
                   lastcheckin: Long)

object MyCheck {
  implicit val jsonDecoder: Decoder[MyCheck] = deriveDecoder
  implicit val jsonEncoder: Encoder[MyCheck] = deriveEncoder
}

case class MyCheckUnmarshable(name:String,
                              timeout: Int,
                              description: String,
                              notification: String)

object MyCheckUnmarshable {
  implicit val jsonDecoder: Decoder[MyCheckUnmarshable] = deriveDecoder
  implicit val jsonEncoder: Encoder[MyCheckUnmarshable] = deriveEncoder
}