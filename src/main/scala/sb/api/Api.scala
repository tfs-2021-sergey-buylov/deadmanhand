package sb.api
import akka.http.scaladsl.server.Route
import sb.MyCheckUnmarshable
import sb.storage.CheckStorageImpl

object Api {
  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
  def listChecks: Route = path("list") {
    get {
      complete(CheckStorageImpl.list())
    }
  }
  def addCheck:Route = path(pm="add"){
    post {
      decodeRequest {
        entity(as[MyCheckUnmarshable]){ r => complete(CheckStorageImpl.addCheck(r.name,r.timeout,r.description,r.notification))}
      }
    }
  }

  def updateTimeout:Route =path("update"/JavaUUID){uuid =>
    get {
      complete(CheckStorageImpl.updateTimeout(uuid))
    }
  }

  def delCheck:Route =path("del"/JavaUUID){uuid =>
    delete {
      complete(CheckStorageImpl.delCheck(uuid))
    }
  }

  val route: Route = pathPrefix("api") {
    listChecks ~ addCheck ~ updateTimeout ~ delCheck
  }
}
