package sb.logic
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import sb.DeadManHttpApp.ac
import sb.MyCheck
import sb.storage.CheckStorageImpl

import java.util.concurrent.{ScheduledThreadPoolExecutor, TimeUnit}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.concurrent.ExecutionContext.Implicits.global

object Scheduler {
  private val javaScheduler = new ScheduledThreadPoolExecutor(1)

  def executeAfter[U](delay: FiniteDuration)(block: => U): Cancellable = {
    val sf = javaScheduler.schedule(() => block, delay.toMillis, TimeUnit.MILLISECONDS)
    () => {
      sf.cancel(false)
      ()
    }
  }
}
trait Cancellable {
  def cancel(): Unit
}

object Checker {
  def sendNotificationtg(chatid:Long,message:String)= {
    val tgtoken = sys.env.get("TG_TOKEN").get
    val body:String= s"""{
        "chat_id":-1001293046572,
        "text":"$message",
        "disable_notification":false
    }"""
    val r=HttpRequest(
      method = HttpMethods.POST,
      uri = s"https://api.telegram.org/$tgtoken/sendMessage",
      entity = HttpEntity(ContentTypes.`application/json`,body)
    )
    Http().singleRequest(r)
  }
  def getNotificationList:Future[Seq[MyCheck]]= {
    for {
      list<-CheckStorageImpl.list()
    }yield list.filter(x=>((System.currentTimeMillis()-x.lastcheckin))>x.timeout)
  }

  def processNotification(checks:Future[Seq[MyCheck]])= {
    for {
      x <- checks
    } yield x.map(x => sendNotificationtg(x.notification.toLong,x.description))
  }
  //"-1001293046572"
  def badMainLoop =
    Future{
      while (true) {
        Thread.sleep(60000)
        println(System.currentTimeMillis())
        Await.result(processNotification(getNotificationList),Duration.Inf)
      }
    }
}

