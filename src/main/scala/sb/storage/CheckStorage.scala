package sb.storage
import sb.MyCheck
import java.util.UUID
import scala.concurrent.Future


trait CheckStorage {


  def list(): Future[Seq[MyCheck]]

  def addCheck(name: String, timeout: Int, description: String, notification: String): Future[Unit]

  def updateTimeout(uuid: UUID): Future[Int]

  def delCheck(uuid: UUID): Future[Int]
}