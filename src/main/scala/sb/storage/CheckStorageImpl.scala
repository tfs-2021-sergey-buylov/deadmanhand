package sb.storage
import sb.MyCheck
import slick.jdbc.SQLiteProfile.api._
import java.util.UUID
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class Data(tag: Tag) extends Table[(UUID,String,Int,String,String,Long)](tag, "Data") {
  def uuid = column[UUID]("uuid", O.PrimaryKey)
  def name = column[String]("name")
  def timeout = column[Int]("timeout")
  def description = column[String]("description")
  def notification = column[String]("notification")
  def lastcheckin:Rep[Long] = column[Long]("lastcheckin")
  def * = (uuid,name,timeout,description,notification,lastcheckin)
}

object CheckStorageImpl extends CheckStorage{

  val table = TableQuery[Data]
  /*
  val db = Database.forConfig("movies")
  val setup = DBIO.seq(
    (table.schema).create
  )
  db.run(setup).onComplete(println)

   */


  override def list(): Future[Seq[MyCheck]] = {
    val db = Database.forConfig("db")
    try {
      for {
        r <- db.run(table.result)
      } yield r.map(x => MyCheck(x._1, x._2, x._3, x._4, x._5,x._6))
    }finally db.close()
  }

  override def addCheck(name:String,timeout: Int, description: String, notification: String): Future[Unit] = {
    val db = Database.forConfig("db")
    try {
      db.run(DBIO.seq(table +=((UUID.randomUUID(),name,timeout,description,notification,System.currentTimeMillis()))))
    }finally db.close()
  }


  override def updateTimeout(uuid: UUID): Future[Int] = {
    val db = Database.forConfig("db")
    try {
      db.run(table.filter(_.uuid===uuid).map(_.lastcheckin)
        .update(System.currentTimeMillis())).filter(_>0)
    }finally db.close()
  }

  override def delCheck(uuid: UUID) = {
    val db = Database.forConfig("db")
    try {
      db.run(table.filter(_.uuid===uuid).delete)
    } finally db.close()
  }
}
